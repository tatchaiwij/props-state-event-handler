import './App.css';
import React from 'react';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <div id='usercontainer' style={{ display: 'none' }} >
          <img id='picture' alt='Avatar' />
          <p id='email'></p>
          <p id='gender'></p>
          <p id='name'></p>
        </div>
        <button onClick={Fetch}>Generate User</button>
      </header>
    </div>
  );
}

function Fetch() {
  fetch('https://randomuser.me/api/')
    .then(response => response.json())
    .then(data => {
      document.getElementById('picture').src = data.results[0].picture.large;
      document.getElementById('email').innerHTML = 'email: ' + data.results[0].email;
      document.getElementById('gender').innerHTML = 'gender: ' + data.results[0].gender;
      document.getElementById('name').innerHTML = data.results[0].name.title + ' ' + data.results[0].name.first + ' ' + data.results[0].name.last;
      document.getElementById('usercontainer').style.display = 'block';
    })
}

export default App;

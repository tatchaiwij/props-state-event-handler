export default function Card({ background }) {
    return (
      <div
        className="card"
        style={{
          background,
          border: "1px solid gray"
        }}
        onClick={(e) => alert('This card is ' + background + '.')}
      ></div>
    );
  }
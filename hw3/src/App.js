import React from "react";
import Card from './Card';
import './App.css';

const color = ["red", "blue", "green", "purple", "pink"];

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      // shuffle state
      decks: color.concat(color)
    };
  }

  shuffle = ([...arr]) => {
    // shuffle logic
    let m = arr.length;
    while (m) {
      const i = Math.floor(Math.random() * m--);
      [arr[m], arr[i]] = [arr[i], arr[m]];
    }
    return arr;
  };

  handleShuffle = () => {
    // shuffle card
    this.setState({ decks: this.shuffle(this.state.decks) })
  };

  componentDidMount (){
    this.handleShuffle();
  }

  render() {
    return (
      <div>
        <div className="card-wrap">
          {/* loop card */
            this.state.decks.map((cardcolor, index) => (
              <Card key={index} background={cardcolor} />
            ))
          }
        </div>
        <div className="text-center">
          <button onClick={this.handleShuffle}>new deck</button>
        </div>
      </div>
    );
  }
}
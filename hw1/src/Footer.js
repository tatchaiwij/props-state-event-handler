import styled from 'styled-components'

const Foot = styled.footer `
    color: white;
    background-color: #303030;
    width:100%;
    height:60px;  
`;

const Footername = styled.p `
    margin-left: auto;
    margin-right: 10px;
`;

const Container = styled.div `
    display: flex;
    height: 100%;
    justify-content: space-between;
`;

function Footer() {
    return (
        <Foot>
            <Container>
                <Footername>Tatchai Wijitwiengrat</Footername>
            </Container>
        </Foot>
    )
}

export default Footer;
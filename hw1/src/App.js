import Navbar from './Navbar';
import Picturecard from './Picturecard'
import Footer from './Footer';

import styled from 'styled-components'

const current = new Date();
const getdate = `${current.getDate()}/${current.getMonth() + 1}/${current.getFullYear()}`;

const items = [
  {
    id: 0,
    date: getdate,
    likeCount: Math.round(0 + (Math.random() * 100)),
    commentCount: Math.round(0 + (Math.random() * 100)),
    imgSrc: 'https://unsplash.it/150/200?image=' + Math.round(0 + (Math.random() * 100)),
    createBy: 'Prasert Intira',
  },
  {
    id: 1,
    date: getdate,
    likeCount: Math.round(0 + (Math.random() * 100)),
    commentCount: Math.round(0 + (Math.random() * 100)),
    imgSrc: 'https://unsplash.it/150/200?image=' + Math.round(0 + (Math.random() * 100)),
    createBy: 'Kanda Kulap',
  },
  {
    id: 2,
    date: getdate,
    likeCount: Math.round(0 + (Math.random() * 100)),
    commentCount: Math.round(0 + (Math.random() * 100)),
    imgSrc: 'https://unsplash.it/150/200?image=' + Math.round(0 + (Math.random() * 100)),
    createBy: 'Mongkut Arthit',
  },
  {
    id: 3,
    date: getdate,
    likeCount: Math.round(0 + (Math.random() * 100)),
    commentCount: Math.round(0 + (Math.random() * 100)),
    imgSrc: 'https://unsplash.it/150/200?image=' + Math.round(0 + (Math.random() * 100)),
    createBy: 'Pranee Ubon',
  },
  {
    id: 4,
    date: getdate,
    likeCount: Math.round(0 + (Math.random() * 100)),
    commentCount: Math.round(0 + (Math.random() * 100)),
    imgSrc: 'https://unsplash.it/150/200?image=' + Math.round(0 + (Math.random() * 100)),
    createBy: 'Arthit Somsak',
  },
]

function App() {
  const itemschunk = chunkArrayInGroups(items, 4);
  const Table = styled.table`
    margin-top: 10px;
    margin-right: auto;
    margin-left: auto;
  `;

  const Body = styled.div`
  background-color: #161B22;
  `;
  return (
    <Body>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"></link>
      <Navbar />
      <Table>
        <tbody>
          {itemschunk.map(chunk => {
            return (<tr key={Math.random()}>
              {chunk.map(item => {
                return <td key={item.id}><Picturecard id={item.id} date={item.date} likeCount={item.likeCount} commentCount={item.commentCount} imgSrc={item.imgSrc} createBy={item.createBy} /></td>
              })}
            </tr>)
          })}
        </tbody>
      </Table>
      <Footer />
    </Body>
  );
}

function chunkArrayInGroups(arr, size) {
  var myArray = [];
  for (var i = 0; i < arr.length; i += size) {
    myArray.push(arr.slice(i, i + size));
  }
  return myArray;
}

export default App;

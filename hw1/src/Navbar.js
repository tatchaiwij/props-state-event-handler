import styled from 'styled-components'

import logo from './pikkanode.png';

const Nav = styled.header `
    height: 40px;
    padding: 10px;
    background-color: #303030;
`;

const Container = styled.div `
    display: flex;
    height: 100%;
    justify-content: space-between;
`;

const Buttoncontainer = styled.div `
    width: fit-content;
    display: flex;
    height: 100%;
    justify-content: space-between;
`;

const Logo = styled.img `
    height: 100%;
    margin-left: 10px;
`;

const Headerbutton = styled.button `
    margin-left: 20px;
    background-color: #303030;
    border: none;
    color: white;
    cursor: pointer;
`;

function Navbar() {
    return (
        <Nav>
            <Container>
                <Logo src={logo} alt="logo" />
                <Buttoncontainer>
                    <Headerbutton>Create pikka</Headerbutton>
                    <Headerbutton>Sign-up</Headerbutton>
                    <Headerbutton>Sign-in</Headerbutton>
                    <Headerbutton>Sign-out</Headerbutton>
                </Buttoncontainer>
            </Container>
        </Nav>
    )
}

export default Navbar;